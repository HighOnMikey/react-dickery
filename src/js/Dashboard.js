import TopMenuBar from './nav/TopMenuBar';

var menuData = {
    top: [
        {
            align: "left",
            items: [
                { type: "link", href: "#", content: "Link"}
            ]
        },
        {
            align: "right",
            items: [
                { type: "dropdown", href: "#", content: "", icon: {name: "cog", size: "lg"}, items: [
                    { type: "link", href: "#", content: "Link"},
                    { type: "divider"},
                    { type: "link", href: "#", content: "Link"}
                ]}
            ]
        }
    ]
};

var Dashboard = React.createClass({
    render: function() {
        return (
            <div className="container-fluid">
                <TopMenuBar menus={menuData.top}/>
                <p>Hey</p>
            </div>
        );
    }
});

ReactDOM.render(<Dashboard/>, document.getElementById("dashboard"));