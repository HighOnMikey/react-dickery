export default React.createClass({
    render: function() {
        var className = "fa fa-" + this.props.name;

        if (this.props.size) {
            className += " fa-" + this.props.size;
        }

        return (
            <i className={className}/>
        );
    }
});