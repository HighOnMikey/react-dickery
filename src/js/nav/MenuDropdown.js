import MenuLink from './MenuLink';
import MenuDivider from './MenuDivider';

export default React.createClass({
    render: function() {
        var className = "dropdown-menu";

        var items = this.props.items.map(function (item) {
            if (item.type === "link") {
                return (
                    <MenuLink type={'dropdown-' + item.type} href={item.href} content={item.content} icon={item.icon}/>
                );
            }

            if (item.type === "divider") {
                return (<MenuDivider/>);
            }
        });

        if (this.props.align === "right") {
            className += " dropdown-menu-right";
        }

        return (
            <div className={className}>
                {items}
            </div>
        );
    }
});