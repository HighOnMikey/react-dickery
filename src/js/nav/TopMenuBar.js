import MenuItem from './MenuItem';

export default React.createClass({
    render: function() {
        var menus = this.props.menus.map(function (menu) {
            var menuItems = menu.items.map(function(item) {
                return (<MenuItem type={item.type} href={item.href} content={item.content} icon={item.icon} items={item.items} align={menu.align}/>);
            });

            var className = "nav navbar-nav";

            if (menu.align && menu.align === "right") {
                className += " pull-right";
            }

            return (
                <ul className={className}>
                    {menuItems}
                </ul>
            );
        });


        return (
            <nav className="navbar navbar-fixed-top navbar-dark bg-inverse">
                <a className="navbar-brand" href="#">Dashboard</a>
                {menus}
            </nav>
        );
    }
});