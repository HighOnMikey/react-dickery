import MenuLink from './MenuLink';
import MenuDropdown from './MenuDropdown';

export default React.createClass({
    render: function() {
        var className = "nav-item",
            dropdown;

        if (this.props.type === "dropdown") {
            className += " dropdown";
            dropdown = <MenuDropdown items={this.props.items} align={this.props.align}/>;
        }
        console.log(this.props.items);
        return (
            <li className={className}>
                <MenuLink type={this.props.type} href={this.props.href} content={this.props.content} icon={this.props.icon}/>
                {dropdown}
            </li>
        );
    }
});