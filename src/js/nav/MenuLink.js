import Icon from '../Icon';

export default React.createClass({
    render: function() {
        var className = "nav-link",
            dataToggle,
            role,
            icon;

        if (this.props.icon) {
            var size = this.props.icon.size ? this.props.icon.size : null;
            icon = <Icon name={this.props.icon.name} size={size}/>;
        }

        if (this.props.type === "dropdown") {
            className += " dropdown-toggle";
            dataToggle = "dropdown";
            role = "button";
        }

        if (this.props.type === "dropdown-link") {
            className = "dropdown-item";
        }

        return (
            <a className={className} href={this.props.href} data-toggle={dataToggle} role={role}>{icon}{this.props.content}</a>
        );
    }
});